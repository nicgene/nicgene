![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-blue?style=flat-square&logo=visualstudiocode&logoColor=white)
![Git](https://img.shields.io/badge/-Git-f44d27?style=flat-square&logo=git&logoColor=white)
![GitLab](https://img.shields.io/badge/-GitLab-303030?style=flat-square&logo=gitlab&logoColor=white)
![GitHub](https://img.shields.io/badge/-GitHub-181717?style=flat-square&logo=github&logoColor=white)
![npm](https://img.shields.io/badge/-npm-CB3837?style=flat-square&logo=npm&logoColor=white)
![TypeScript](https://img.shields.io/badge/-TypeScript-2f74c0?style=flat-square&logo=typescript&logoColor=white)
![JavaScript](https://img.shields.io/badge/-JavaScript-efd81d?style=flat-square&logo=javascript&logoColor=gray)
![V8](https://img.shields.io/badge/-V8-4987ed?style=flat-square&logo=v8&logoColor=white)
![Composer](https://img.shields.io/badge/-Composer-89552c?style=flat-square&logo=composer&logoColor=white)
![PHP](https://img.shields.io/badge/-PHP-777bb3?style=flat-square&logo=php&logoColor=white)
![Python](https://img.shields.io/badge/-Python-f2c740?style=flat-square&logo=python&logoColor=gray)
![Ruby](https://img.shields.io/badge/-Ruby-e21722?style=flat-square&logo=ruby&logoColor=white)
![Node.js](https://img.shields.io/badge/-Node.js-75ad5f?style=flat-square&logo=Nodedotjs&logoColor=white)
![Apache](https://img.shields.io/badge/-Apache-d22129?style=flat-square&logo=apache&logoColor=white)
![NGINX](https://img.shields.io/badge/-NGINX-009639?style=flat-square&logo=nginx&logoColor=white)
![SQL](https://img.shields.io/badge/-SQL-003343?style=flat-square&logo=mariadb&logoColor=white)
![NoSQL](https://img.shields.io/badge/-NoSQL-006346?style=flat-square&logo=mongodb&logoColor=white)
![OAuth 2.0](https://img.shields.io/badge/-OAuth%202.0-000000?style=flat-square&logo=auth0&logoColor=white)
![JSON Web Token](https://img.shields.io/badge/-JWT-ffffff?style=flat-square&logo=monkeytie&logoColor=black)
![GraphQL](https://img.shields.io/badge/-GraphQL-de33a6?style=flat-square&logo=graphql&logoColor=white)
![REST](https://img.shields.io/badge/-REST-f24600?style=flat-square&logo=zapier&logoColor=white)
![Nuxt](https://img.shields.io/badge/-Nuxt-00dc82?style=flat-square&logo=nuxtdotjs&logoColor=white)
![AngularJS](https://img.shields.io/badge/-Angular.js-dd1b16?style=flat-square&logo=angularjs&logoColor=white)
![Backbone.js](https://img.shields.io/badge/-Backbone.js-0071b5?style=flat-square&logo=backbonedotjs&logoColor=white)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-8d12fc?style=flat-square&logo=bootstrap&logoColor=white)
![HTML5](https://img.shields.io/badge/-HTML5-d84924?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-2449d8?style=flat-square&logo=css3&logoColor=white)
![Sass](https://img.shields.io/badge/-Sass-cf649a?style=flat-square&logo=Sass&logoColor=white)
![Photoshop](https://img.shields.io/badge/-Photoshop-001c33?style=flat-square&logo=adobephotoshop&logoColor=white)
![WordPress](https://img.shields.io/badge/-WordPress-1f6f93?style=flat-square&logo=wordpress&logoColor=white)
![Drupal](https://img.shields.io/badge/-Drupal-0094d3?style=flat-square&logo=drupal&logoColor=white)
![Serverless](https://img.shields.io/badge/-Serverless-fd5750?style=flat-square&logo=serverless&logoColor=white)
![Docker](https://img.shields.io/badge/-Docker-2496ed?style=flat-square&logo=docker&logoColor=white)
![Kubernetes](https://img.shields.io/badge/-Kubernetes-326de6?style=flat-square&logo=kubernetes&logoColor=white)
![Cloudflare](https://img.shields.io/badge/-Cloudflare-f6821f?style=flat-square&logo=cloudflare&logoColor=white)
![DigitalOcean](https://img.shields.io/badge/-DigitalOcean-0069ff?style=flat-square&logo=digitalocean&logoColor=white)
![AWS](https://img.shields.io/badge/-AWS-f29100?style=flat-square&logo=amazon&logoColor=white)
![Google Cloud](https://img.shields.io/badge/-Google%20Cloud-3f7ee8?style=flat-square&logo=googlecloud&logoColor=white)
![Arch Linux](https://img.shields.io/badge/-Arch%20Linux-0f94d2?style=flat-square&logo=archlinux&logoColor=white)
![Ubuntu](https://img.shields.io/badge/-Ubuntu-d64613?style=flat-square&logo=ubuntu&logoColor=white)
![CentOS](https://img.shields.io/badge/-CentOS-94c328?style=flat-square&logo=centos&logoColor=white)
![W3C](https://img.shields.io/badge/-W3C-1a6ba6?style=flat-square&logo=w3c&logoColor=white)
![OWASP](https://img.shields.io/badge/-OWASP-000000?style=flat-square&logo=owasp&logoColor=white)
